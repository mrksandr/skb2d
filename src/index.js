import express from 'express';
import cors from 'cors';
import getColorType from './get-color-type';
import convertColor from './convert-color';

const app = express();

const corsOptions = {
  origin: 'http://account.skill-branch.ru',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

app.get('/task2D', (req, res) => {
	try {

		const color = req.query.color.trim(),
			format = getColorType(color),
			convertedColor = convertColor(color, format);
		console.log(convertedColor);
		return res.send(convertedColor);
	} catch (e) {
		res.send('Invalid color');
	}
});


app.listen(3000, () => {
	console.log('Example app listening on port 3000!');
});