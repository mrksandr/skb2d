import convert from 'color-convert';
import colorsDefault from './colors-default';

export default function convertColor(color, format) {
	let result = '';

	switch (format) {
		case 'hex':
			color = color.toLowerCase().replace('#', '');

			if (color.length === 3) {
				color = `${color[0]}${color[0]}${color[1]}${color[1]}${color[2]}${color[2]}`;
			}

			result = `#${color}`;

			break;

		case 'hsl':
			const [allHsl, hue, sat, light] =
					color.match(/^\s*hsl\(\s*(\d{1,3})\D+(\d{1,3})%\D+(\d{1,3})%\s*\)\s*$/),
				hsl = convert.hsl.hex(hue, sat, light).toLowerCase();

			if ((hue >= 0 && hue <= 360) && (sat >= 0 && sat <= 100) && (light >= 0 && light <= 100)) {
				result = `#${hsl}`;
			} else {
				throw new Error('Invalid color');
			}

			break;

		case 'rgb':
			color = color.trim().toLowerCase();

			const [allRgb, red, green, blue] =
					color.match(/^\s*rgb\(\s*(\d{1,3})\D+(\d{1,3})\D+(\d{1,3})\s*\)\s*$/),
				hex = convert.rgb.hex(red, green, blue).toLowerCase();

			if ((red >= 0 && red <= 255) && (green >= 0 && green <= 255) && (blue >= 0 && blue <= 255)) {
				result = `#${hex}`;
			} else {
				throw new Error('Invalid color')
			}

			break;

		case 'word':
			color = color.trim().toLowerCase();
			result = COLOR_LIST[color];

			break;

		default:
			throw new Error('Invalid color');
	}

	return	result;
};