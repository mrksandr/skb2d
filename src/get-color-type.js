import colorsDefault from './colors-default';

export default function getColorType(str) {
	const color = str.trim().toLowerCase();

	//word
	let result = Boolean(colorsDefault[color]);
	let type = 'word';

	if (!result) {
		//hex

		const reHex = new RegExp(/^\s*#?([A-Fa-f0-9]{3}){1,2}\s*$/);
		result = reHex.test(color);
		type = 'hex';
	}

	if (!result) {
		const reHsl = new RegExp(/^\s*hsl\(\s*\d{1,3}\D+\d{1,3}%\D+\d{1,3}%\s*\)\s*$/);
		result = reHsl.test(color);
		type = 'hsl';
	}

	if (!result) {
		//rgb
		const reRgb = new RegExp(/^\s*rgb\(\s*\d{1,3}\D+\d{1,3}\D+\d{1,3}\s*\)\s*$/);
		result = reRgb.test(color);
		type = 'rgb';
	}

	return type;
};